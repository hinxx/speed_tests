#!/bin/bash

# run limited set of tests with different number of loops and sample count

S="300000 600000 1200000"

#T=$(make list | awk '/^TEST/ { print $2; }')
# only use subset of tests here
T="
test_add
test_add_mul
test_div
test_div_sub
test_mult
test_sin
test_sub
test_sr
test_if
test_nested_if
test_copy
"
L="1 10 30 50 100"

if [ -f tests.csv ]; then
	echo "Log tests.csv exists. Use empty one?"
	read ans
	case $ans in
		y|Y)
			mv tests.csv "tests saved on $(date).csv"
	esac
fi

for l in $L; do
	for t in $T; do
		for s in $S; do
			FF=$(find . -name "$t")
			for f in $FF; do
				./$f -L $l -S $s
			done
		done
	done
done

