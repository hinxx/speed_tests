/*
 * test_add_memcpy_locc.c
 *
 *  Created on: Aug 11, 2014
 *      Author: essdev
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "common.h"

static TEST_DTYPE1 stata[SAMPLES];

/* test operation(s); these are executed for each sample in test_case() loop */
#define TEST_OP	{ \
			locc[i] = x[i] + (TEST_DTYPE1) (y[i]); \
}

/* build test_case() */
TEST_CASE_F_LOCC
/* build run() */
TEST_RUN_F
