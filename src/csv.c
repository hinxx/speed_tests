/*
 * csv.c
 *
 *  Created on: Aug 19, 2014
 *      Author: essdev
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include "common.h"
#include "csv.h"

static FILE *fp = NULL;
static char linebuf[4096];
static unsigned int buflen;
static unsigned int id;
static const char *csv_header = "ID|HOST|CASE|DTYPE1|DTYPE2|SAMPLES|LOOPS|AVG|MAX|TOTAL|CPULOAD|NRCPUS|OPTFLAGS|OP\n";

int csv_init(const char *filename) {
	int rc;

	id = 0;
	buflen = 0;

	if (fp == NULL) {
		fp = fopen(filename, "a+");
		assert(fp != NULL);
	}

	fseek(fp, 0L, SEEK_SET);
	errno = 0;
	rc = fread(linebuf, 1, sizeof(linebuf), fp);
	if (rc == 0) {
		if (errno) {
			S("fgets(): %s", strerror(errno));
			return 1;
		}
		rc = fwrite(csv_header, 1, strlen(csv_header), fp);
		assert(rc == strlen(csv_header));
		fflush(fp);
		id = 1;
	} else {
		fseek(fp, 0L, SEEK_SET);
		while ((rc = fgetc(fp)) != EOF) {
			if (rc == '\n') {
				id++;
			}
		}
	}
	//U("CSV ID", id);
	fseek(fp, 0L, SEEK_END);

	return 0;
}

int csv_close() {
	assert(fp != NULL);

	fflush(fp);
	fclose(fp);

	return 0;
}

unsigned int csv_get_id() {
	return id;
}

int csv_append_id() {

	assert(buflen == 0);
	buflen = sprintf(linebuf, "%u", id);
	linebuf[buflen++] = '|';

	return 0;
}

int csv_append_str(const char *token) {

	strcpy(linebuf+buflen, token);
	buflen += strlen(token);
	linebuf[buflen++] = '|';

	return 0;
}

int csv_append_double(const double token) {

	buflen += sprintf(linebuf+buflen, "%.3f", token);
	linebuf[buflen++] = '|';

	return 0;
}

int csv_append_ull(const unsigned long long token) {

	buflen += sprintf(linebuf+buflen, "%llu", token);
	linebuf[buflen++] = '|';

	return 0;
}

int csv_commit() {

	buflen -= 1;
	linebuf[buflen++] = '\n';
	linebuf[buflen++] = '\0';
	fwrite(linebuf, 1, strlen(linebuf), fp);
	buflen = 0;
	fflush(fp);

	return 0;
}

