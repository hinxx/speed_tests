/*
 * common.h
 *
 *  Created on: Aug 8, 2014
 *      Author: essdev
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <errno.h>
#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include <string.h>
#include <libgen.h>
#include <stdint.h>
#include <math.h>

#define GCC_VERSION (__GNUC__ * 10000 \
				   + __GNUC_MINOR__ * 100 \
				   + __GNUC_PATCHLEVEL__)

/* Test for GCC < 4.4.6 */
#if GCC_VERSION <= 40460
#define __builtin_assume_aligned(x,y) (x)
#endif

#ifdef NDEBUG
#define debug(M, ...)
#else
//#define debug(M, ...)	{ do { fprintf(stderr, "%s %s(): " M "\n", __FILE__, __FUNCTION__, ##__VA_ARGS__); } while (0); }
#define debug(M, ...)	{ do { fprintf(stderr, "%s(): " M "\n", __FUNCTION__, ##__VA_ARGS__); } while (0); }
#endif
#define I(x)			debug(x)
#define S(a,b)			debug("%s %s", a, b)
#define U(a,b)			debug("%s %u", a, b)
#define LLU(a,b)		debug("%s %llu", a, b)
#define D(a,b)			debug("%s %.3f", a, b)

#define XSTR(s)			STR(s)
#define STR(s)			#s
#define FILE(s)			basename(s)

/* default array size is 300kb */
#define SAMPLES			300000
#define RAND_HALF		(unsigned int)(RAND_MAX / 2)
#define RAND_QUART		(unsigned int)(RAND_MAX / 4)

double time_msec();
void cpu_load_start();
void cpu_load_end();
long double cpu_load_calc();

extern void run();
extern unsigned long long test_loops;
extern unsigned long long test_size;
extern double test_avg, test_max, test_total;
extern char *test_name, *test_dtype1, *test_dtype2, *test_op;

#define TEST_NAME		FILE(__FILE__)
#ifndef TEST_DTYPE1
#error Missing data type 1 definition
#define TEST_DTYPE1			double	/* keep eclipse happy */
#endif
#ifndef TEST_DTYPE2
#error Missing data type 2 definition
#define TEST_DTYPE2			double	/* keep eclipse happy */
#endif

#define FILL_ARRAY(A, T, L) { \
		unsigned long long __i; \
		srand((unsigned int) time_msec()); \
		for (__i = 0; __i < L; __i++) { \
			A[__i] = (T) (__i * (T)rand()); \
		} \
}

#define CONCATENATE(X,Y) X ( Y )
#define pragma_omp_parallel_for_private(x) \
  _Pragma( XSTR( CONCATENATE(omp parallel for private,x) ) )

#define TEST_CASE_F \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
			TEST_DTYPE1 z; \
			TEST_DTYPE1 *x = a; \
			TEST_DTYPE2 *y = b; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
		}

#define TEST_CASE_F_LOCN \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
			TEST_DTYPE1 z; \
			TEST_DTYPE1 *x = a; \
			TEST_DTYPE2 *y = b; \
			const unsigned long long sz = test_size; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < sz; i++) { \
				TEST_OP \
			} \
		}

#define TEST_CASE_F_REST \
		void test_case(TEST_DTYPE1 *__restrict a, TEST_DTYPE2 *__restrict b) { \
			unsigned long long i; \
			TEST_DTYPE1 z; \
			TEST_DTYPE1 *x = a; \
			TEST_DTYPE2 *y = b; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
		}

#define TEST_CASE_F_LOCA \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
			TEST_DTYPE1 z; \
			TEST_DTYPE1 loca[test_size]; \
			TEST_DTYPE1 *x = loca; \
			TEST_DTYPE2 *y = b; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
		}

#define TEST_CASE_F_LOCB \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
			TEST_DTYPE1 z; \
			TEST_DTYPE2 locb[test_size]; \
			TEST_DTYPE1 *x = a; \
			TEST_DTYPE2 *y = locb; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
		}

#define TEST_CASE_F_LOCAB \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
\
			TEST_DTYPE1 loca[test_size]; \
			TEST_DTYPE2 locb[test_size]; \
			TEST_DTYPE1 *x = loca; \
			TEST_DTYPE2 *y = locb; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
		}

#define TEST_CASE_F_LOCC \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
			TEST_DTYPE1 z; \
			TEST_DTYPE1 locc[test_size]; \
			TEST_DTYPE1 *x = a; \
			TEST_DTYPE2 *y = b; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
			memcpy(stata, locc, sizeof(TEST_DTYPE1) * SAMPLES); \
		}

#define TEST_CASE_F_STATA \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
\
			TEST_DTYPE1 *x = stata; \
			TEST_DTYPE2 *y = b; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
		}

#define TEST_CASE_F_STATB \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
\
			TEST_DTYPE1 *x = a; \
			TEST_DTYPE2 *y = statb; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
		}

#define TEST_CASE_F_STATAB \
		void test_case(TEST_DTYPE1 *a, TEST_DTYPE2 *b) { \
			unsigned long long i; \
\
			TEST_DTYPE1 *x = stata; \
			TEST_DTYPE2 *y = statb; \
\
			/* omp pragmas will only be used when -fopenmp is passed to compiler */ \
			pragma_omp_parallel_for_private(i) \
			for (i = 0; i < test_size; i++) { \
				TEST_OP \
			} \
		}


#define TEST_RUN_F \
		void run() { \
			unsigned long long i; \
			double s, e, m, t, d; \
\
			test_name = TEST_NAME; \
			test_dtype1 = XSTR(TEST_DTYPE1); \
			test_dtype2 = XSTR(TEST_DTYPE2); \
			test_op = XSTR(TEST_OP); \
\
			/* on 64-bit systems malloc() block address will always be aligned to 16 */ \
			TEST_DTYPE1 *a = (TEST_DTYPE1 *)malloc((test_size) * sizeof(TEST_DTYPE1)); \
			TEST_DTYPE2 *b = (TEST_DTYPE2 *)malloc((test_size) * sizeof(TEST_DTYPE2)); \
\
			FILL_ARRAY(a, TEST_DTYPE1, test_size); \
			FILL_ARRAY(b, TEST_DTYPE2, test_size); \
\
			t = m = 0; \
			/* run the test for desired number of loops */ \
			for (i = 0; i < test_loops; i++) { \
				s = time_msec(); \
\
				/* run the test function */ \
				test_case(a, b); \
\
				e = time_msec(); \
				d = e - s; \
				t += d; \
				if (m < d) m = d; \
			} \
\
			test_total = t; \
			test_avg = t / test_loops; \
			test_max = m; \
\
			free(a); \
			free(b); \
		} \


#endif /* COMMON_H_ */
