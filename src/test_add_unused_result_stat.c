/*
 * test_add_unused_result_stat.c
 *
 *  Created on: Aug 11, 2014
 *      Author: essdev
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "common.h"

static TEST_DTYPE1 statz;

/* test operation(s); these are executed for each sample in test_case() loop */
#define TEST_OP	{ \
			statz = x[i] + (TEST_DTYPE1) (y[i]); \
}

/* build test_case() */
TEST_CASE_F
/* build run() */
TEST_RUN_F
