/*
 * csv.h
 *
 *  Created on: Aug 19, 2014
 *      Author: essdev
 */

#ifndef CSV_H_
#define CSV_H_

int csv_init(const char *filename);
int csv_close();
int csv_append_id();
int csv_append_str(const char *token);
int csv_append_double(const double token);
int csv_append_ull(const unsigned long long token);
int csv_commit();
unsigned int csv_get_id();

#endif /* CSV_H_ */
