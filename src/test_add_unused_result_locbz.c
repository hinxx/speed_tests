/*
 * test_add_unused_result_locbz.c
 *
 *  Created on: Aug 11, 2014
 *      Author: essdev
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "common.h"

/* test operation(s); these are executed for each sample in test_case() loop */
#define TEST_OP	{ \
			z = x[i] + (TEST_DTYPE1) (y[i]); \
}

/* build test_case() */
TEST_CASE_F_LOCB
/* build run() */
TEST_RUN_F
