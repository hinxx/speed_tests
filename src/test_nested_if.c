/*
 * test_nested_if.c
 *
 *  Created on: Aug 11, 2014
 *      Author: essdev
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "common.h"

/* test operation(s); these are executed for each sample in test_case() loop */
#define TEST_OP	{ \
			if (x[i] > RAND_HALF) { \
				if (x[i] > (RAND_HALF + RAND_QUART)) { \
					x[i] += (TEST_DTYPE1) (y[i]); \
				} else { \
					x[i] -= (TEST_DTYPE1) (y[i]); \
				} \
			} else { \
				if (x[i] > RAND_QUART) { \
					y[i] += (TEST_DTYPE2) (x[i]); \
				} else { \
					y[i] -= (TEST_DTYPE2) (x[i]); \
				} \
			} \
}

/* build test_case() */
TEST_CASE_F
/* build run() */
TEST_RUN_F
