/*
 * test_add_mul.c
 *
 *  Created on: Aug 11, 2014
 *      Author: essdev
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "common.h"

/* test operation(s); these are executed for each sample in test_case() loop */
#define TEST_OP	{ \
			x[i] += (TEST_DTYPE1) ((TEST_DTYPE1) y[i] * 911.0 + 23.0); \
}

/* build test_case() */
TEST_CASE_F
/* build run() */
TEST_RUN_F
