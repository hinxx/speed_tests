/*
 * common.c
 *
 *  Created on: Aug 8, 2014
 *      Author: essdev
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include "common.h"

static long double a[4], b[4];

double time_msec() {
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (double) ts.tv_sec * 1.0e3 + (double) ts.tv_nsec / 1.0e6;
}

static void cpu_load(long double *d) {
	FILE *fp;
	int ret;

	fp = fopen("/proc/stat","r");
	ret = fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&d[0],&d[1],&d[2],&d[3]);
	if (ret != 4) {
		S("ERROR", "sscanf() failed");
	}
	fclose(fp);
}

void cpu_load_start() {
	cpu_load(a);
}

void cpu_load_end() {
	cpu_load(b);
}

long double cpu_load_calc() {
	return ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
}
