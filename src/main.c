/*
 * main.c
 *
 *  Created on: Aug 11, 2014
 *      Author: essdev
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <sys/mman.h>

#include "common.h"
#include "csv.h"

unsigned long long test_loops;
unsigned long long test_size;
double test_avg, test_max, test_total;
char *test_name, *test_dtype1, *test_dtype2, *test_op;

/* these are defined at compile time and passed as -D to gcc */
#ifdef _OPT_FLAGS
#define OPT_FLAGS XSTR(_OPT_FLAGS)
#else
#define OPT_FLAGS "unknown"
#endif

int main(int argc, char **argv) {
	char c;
	long double loadavg;
	long nrcpus;
	char hostname[128] = {0};

	test_loops = 30;
	test_size = SAMPLES;

	mlockall(MCL_CURRENT | MCL_FUTURE);

	while ((c = getopt(argc, argv, "hL:S:C:")) != (char)-1) {
		switch (c) {
		case 'L':
			sscanf(optarg, "%llu", &test_loops);
			break;
		case 'S':
			sscanf(optarg, "%llu", &test_size);
			break;
		case '?':
		case 'h':
		default:
			printf(
					"Usage: %s [-h] [-L loops] [-S size]\n", argv[0]);
			printf("   \n");
			printf("    -S unsigned int      Sample count (default: %u) \n", SAMPLES);
			printf("    -L unsigned int      Number of loops (default: 30) \n");
			printf("   \n");
			printf("    -h                   Print this message \n");
			printf("   \n");
			return -1;
		}
	}

	S("START", argv[0]);
	nrcpus = sysconf(_SC_NPROCESSORS_ONLN);
	gethostname(hostname, sizeof(hostname));

	csv_init("tests.csv");

	test_avg = -1.0;
	test_max = -1.0;
	test_total = -1.0;
	test_name = "unknown";
	test_dtype1 = "unknown";
	test_dtype2 = "unknown";
	test_op = "unknown";

	cpu_load_start();

	/* run the test */
	run();

	cpu_load_end();
	loadavg = cpu_load_calc();
	loadavg = loadavg * nrcpus * 100.0;

	S("HOSTNAME", hostname);
	U("RUNID", csv_get_id());
	U("NR CPUS", (unsigned int)nrcpus);
	LLU("LOOPS", test_loops);
	LLU("SAMPLES", test_size);
	S("NAME", test_name);
	S("DTYPE1", test_dtype1);
	S("DTYPE2", test_dtype2);
	S("OPT_FLAGS", OPT_FLAGS);
	S("OP", test_op);
	D("AVG [ms]", test_avg);
	D("MAX [ms]", test_max);
	D("TOTAL [ms]", test_total);
	U("CPU LOAD [%]", (unsigned int)loadavg);

	/* create CSV line in the log file - see csv_header for order of tokens */
	csv_append_id();
	csv_append_str(hostname);
	csv_append_str(test_name);
	csv_append_str(test_dtype1);
	csv_append_str(test_dtype2);
	csv_append_ull(test_size);
	csv_append_ull(test_loops);
	csv_append_double(test_avg);
	csv_append_double(test_max);
	csv_append_double(test_total);
	csv_append_ull(loadavg);
	csv_append_ull(nrcpus);
	csv_append_str(OPT_FLAGS);
	csv_append_str(test_op);
	csv_commit();
	csv_close();

	S("FINISH", argv[0]);

	munlockall();
	return 0;
}
