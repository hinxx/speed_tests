#!/bin/bash

# run single test with different number of loops and sample count

S="300000 600000 1200000"

# only use a command line provided test
T="$1"
L="1 10 30 50 100"

if [ -z "$T" ]; then
	echo "Usage: $0 <test name>"
	echo "List of tests:"
	make list | awk '/TEST:/ { printf("  %s\n", $2); }'
	exit 1
fi

if [ -f tests.csv ]; then
	echo "Log tests.csv exists. Use empty one?"
	read ans
	case $ans in
		y|Y)
			mv tests.csv "tests saved on $(date).csv"
	esac
fi

for l in $L; do
	for t in $T; do
		for s in $S; do
			FF=$(find . -name "$t")
			for f in $FF; do
				./$f -L $l -S $s
			done
		done
	done
done

