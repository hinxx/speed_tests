MACH=$(shell $(CC) -dumpmachine)
$(info Using GCC for machine $(MACH))

SUBDIRS :=  \
	noopt \
	O3 \
	Os

# Only when not on PPC (IFC1210) CPU
ifneq ($(MACH),powerpc-linux-gnuspe)

SUBDIRS += native \
			openmp

endif

# parallel make
NUMCPUS := $(shell grep -c '^processor' /proc/cpuinfo)
$(info Using $(NUMCPUS) CPUs in make process)

all:
	@for ii in $(SUBDIRS); do \
		cp rules/Makefile $$ii/Makefile; \
		$(MAKE) $@ -j$(NUMCPUS) --load-average=$(NUMCPUS) -C $$ii -S $@; \
	done

clean:
	@for ii in $(SUBDIRS); do \
		cp rules/Makefile $$ii/Makefile; \
		$(MAKE) $@ -j$(NUMCPUS) --load-average=$(NUMCPUS) -C $$ii -S $@; \
	done
	@for ii in $(SUBDIRS); do \
		rm -f $$ii/Makefile; \
	done

list:
	@echo List of known tests:
	@for ii in src/test*.c; do \
		echo "TEST: $$(basename $$ii .c)"; \
	done
