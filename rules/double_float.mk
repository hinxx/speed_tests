
all: build-double_float
clean: clean-double_float

build-double_float:
	@mkdir -p double_float
	@echo 'TOP := ../..' > double_float/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> double_float/Makefile
	@echo 'include ../flags.mk' >> double_float/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=double' >> double_float/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=float' >> double_float/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> double_float/Makefile
	@+make -C double_float

clean-double_float:
	@rm -fr double_float
