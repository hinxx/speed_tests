
SRCS := $(wildcard $(TOP)/src/*.c)
SRCS_ := $(patsubst $(TOP)/src/%.c,%.c,$(SRCS))
NOTARGET_ := $(filter-out test_%.c,$(SRCS_))
TARGET_SRCS := $(filter test_%.c,$(SRCS_))
TARGETS := $(patsubst %.c,%,$(TARGET_SRCS))
OBJS := $(SRCS_:.c=.o)
NOTARGET_OBJS := $(NOTARGET_:.c=.o)
#$(info TARGETS: $(TARGETS))
#$(info SRCS_: $(SRCS_))
#$(info NOTARGET_: $(NOTARGET_))
#$(info TARGET_SRCS: $(TARGET_SRCS))
#$(info NOTARGET_OBJS: $(NOTARGET_OBJS))
#$(info SRCS: $(SRCS))
#$(info OBJS: $(OBJS))
#$(info CURDIR: $(CURDIR))
#$(error foo)

CFLAGS :=
CFLAGS += -save-temps 
CFLAGS += -Wall -Wno-unknown-pragmas -Wno-unused-variable -Wno-unused-but-set-variable
CFLAGS += -std=gnu99

LDFLAGS := -lrt -lpthread -lm

target-build: gcc_infos $(OBJS) $(TARGETS)

gcc_infos:
	@echo "int main() { return 0; }" | gcc $(filter-out -save-temps,$(CFLAGS)) -v -Q -x c - > gcc_infos 2>&1

%.o: $(TOP)/src/%.c
	$(CC) -c $(CFLAGS) $< -o $@

$(TARGETS): $(OBJS)
	$(CC) $(CFLAGS) $(NOTARGET_OBJS) ${@}.o -o $@ $(LDFLAGS)

target-clean:
	@rm -f -- *.i *.s *.o $(TARGETS) gcc_infos a.out

.PHONY: target-build target-clean
