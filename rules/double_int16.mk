
all: build-double_int16
clean: clean-double_int16

build-double_int16:
	@mkdir -p double_int16
	@echo 'TOP := ../..' > double_int16/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> double_int16/Makefile
	@echo 'include ../flags.mk' >> double_int16/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=double' >> double_int16/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=int16_t' >> double_int16/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> double_int16/Makefile
	@+make -C double_int16

clean-double_int16:
	@rm -fr double_int16
