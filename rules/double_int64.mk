
all: build-double_int64
clean: clean-double_int64

build-double_int64:
	@mkdir -p double_int64
	@echo 'TOP := ../..' > double_int64/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> double_int64/Makefile
	@echo 'include ../flags.mk' >> double_int64/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=double' >> double_int64/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=int64_t' >> double_int64/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> double_int64/Makefile
	@+make -C double_int64

clean-double_int64:
	@rm -fr double_int64
