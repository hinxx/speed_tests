
all: build-float_int32
clean: clean-float_int32

build-float_int32:
	@mkdir -p float_int32
	@echo 'TOP := ../..' > float_int32/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> float_int32/Makefile
	@echo 'include ../flags.mk' >> float_int32/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=float' >> float_int32/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=int32_t' >> float_int32/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> float_int32/Makefile
	@+make -C float_int32

clean-float_int32:
	@rm -fr float_int32
