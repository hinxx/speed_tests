
all: build-float_uint64
clean: clean-float_uint64

build-float_uint64:
	@mkdir -p float_uint64
	@echo 'TOP := ../..' > float_uint64/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> float_uint64/Makefile
	@echo 'include ../flags.mk' >> float_uint64/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=float' >> float_uint64/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=uint64_t' >> float_uint64/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> float_uint64/Makefile
	@+make -C float_uint64

clean-float_uint64:
	@rm -fr float_uint64
