
all: build-double_uint64
clean: clean-double_uint64

build-double_uint64:
	@mkdir -p double_uint64
	@echo 'TOP := ../..' > double_uint64/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> double_uint64/Makefile
	@echo 'include ../flags.mk' >> double_uint64/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=double' >> double_uint64/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=uint64_t' >> double_uint64/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> double_uint64/Makefile
	@+make -C double_uint64

clean-double_uint64:
	@rm -fr double_uint64
