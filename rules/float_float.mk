
all: build-float_float
clean: clean-float_float

build-float_float:
	@mkdir -p float_float
	@echo 'TOP := ../..' > float_float/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> float_float/Makefile
	@echo 'include ../flags.mk' >> float_float/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=float' >> float_float/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=float' >> float_float/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> float_float/Makefile
	@+make -C float_float

clean-float_float:
	@rm -fr float_float
