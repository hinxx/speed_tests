
all: build-float_int16
clean: clean-float_int16

build-float_int16:
	@mkdir -p float_int16
	@echo 'TOP := ../..' > float_int16/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> float_int16/Makefile
	@echo 'include ../flags.mk' >> float_int16/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=float' >> float_int16/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=int16_t' >> float_int16/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> float_int16/Makefile
	@+make -C float_int16

clean-float_int16:
	@rm -fr float_int16
