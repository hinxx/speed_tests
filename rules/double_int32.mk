
all: build-double_int32
clean: clean-double_int32

build-double_int32:
	@mkdir -p double_int32
	@echo 'TOP := ../..' > double_int32/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> double_int32/Makefile
	@echo 'include ../flags.mk' >> double_int32/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=double' >> double_int32/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=int32_t' >> double_int32/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> double_int32/Makefile
	@+make -C double_int32

clean-double_int32:
	@rm -fr double_int32
