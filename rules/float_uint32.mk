
all: build-float_uint32
clean: clean-float_uint32

build-float_uint32:
	@mkdir -p float_uint32
	@echo 'TOP := ../..' > float_uint32/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> float_uint32/Makefile
	@echo 'include ../flags.mk' >> float_uint32/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=float' >> float_uint32/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=uint32_t' >> float_uint32/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> float_uint32/Makefile
	@+make -C float_uint32

clean-float_uint32:
	@rm -fr float_uint32
