
all: build-double_uint16
clean: clean-double_uint16

build-double_uint16:
	@mkdir -p double_uint16
	@echo 'TOP := ../..' > double_uint16/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> double_uint16/Makefile
	@echo 'include ../flags.mk' >> double_uint16/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=double' >> double_uint16/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=uint16_t' >> double_uint16/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> double_uint16/Makefile
	@+make -C double_uint16

clean-double_uint16:
	@rm -fr double_uint16
