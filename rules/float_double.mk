
all: build-float_double
clean: clean-float_double

build-float_double:
	@mkdir -p float_double
	@echo 'TOP := ../..' > float_double/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> float_double/Makefile
	@echo 'include ../flags.mk' >> float_double/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=float' >> float_double/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=double' >> float_double/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> float_double/Makefile
	@+make -C float_double

clean-float_double:
	@rm -fr float_double
