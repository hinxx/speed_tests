
all: target-build

clean: target-clean

.PHONY: all clean

# add optimization flags (defined in flags.mk)
CFLAGS += $(OPT_FLAGS)
# set these defines for the test binary itself
CFLAGS += -D_OPT_FLAGS="$(OPT_FLAGS)"
