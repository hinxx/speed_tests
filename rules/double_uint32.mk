
all: build-double_uint32
clean: clean-double_uint32

build-double_uint32:
	@mkdir -p double_uint32
	@echo 'TOP := ../..' > double_uint32/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> double_uint32/Makefile
	@echo 'include ../flags.mk' >> double_uint32/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=double' >> double_uint32/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=uint32_t' >> double_uint32/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> double_uint32/Makefile
	@+make -C double_uint32

clean-double_uint32:
	@rm -fr double_uint32
