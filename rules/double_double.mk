
all: build-double_double
clean: clean-double_double

build-double_double:
	@mkdir -p double_double
	@echo 'TOP := ../..' > double_double/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> double_double/Makefile
	@echo 'include ../flags.mk' >> double_double/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=double' >> double_double/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=double' >> double_double/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> double_double/Makefile
	@+make -C double_double

clean-double_double:
	@rm -fr double_double
