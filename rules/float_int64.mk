
all: build-float_int64
clean: clean-float_int64

build-float_int64:
	@mkdir -p float_int64
	@echo 'TOP := ../..' > float_int64/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> float_int64/Makefile
	@echo 'include ../flags.mk' >> float_int64/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=float' >> float_int64/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=int64_t' >> float_int64/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> float_int64/Makefile
	@+make -C float_int64

clean-float_int64:
	@rm -fr float_int64
