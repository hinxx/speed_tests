
all: build-float_uint16
clean: clean-float_uint16

build-float_uint16:
	@mkdir -p float_uint16
	@echo 'TOP := ../..' > float_uint16/Makefile
	@echo 'include $$(TOP)/rules/pre.mk' >> float_uint16/Makefile
	@echo 'include ../flags.mk' >> float_uint16/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE1=float' >> float_uint16/Makefile
	@echo 'CFLAGS += -DTEST_DTYPE2=uint16_t' >> float_uint16/Makefile
	@echo 'include $$(TOP)/rules/post.mk' >> float_uint16/Makefile
	@+make -C float_uint16

clean-float_uint16:
	@rm -fr float_uint16
