## Speed tests

This is a suite of performance tests that can be used to evaluate
execution speed of basic operations on a given CPU.

List of known tests (obtained using *make list*):

    test_add
    test_add_loca
    test_add_locab
    test_add_locb
    test_add_locn
    test_add_memcpy_locc
    test_add_mul
    test_add_rest
    test_add_stata
    test_add_statab
    test_add_statb
    test_add_unused_result_loc
    test_add_unused_result_locaz
    test_add_unused_result_locbz
    test_add_unused_result_stat
    test_copy
    test_div
    test_div_sub
    test_if
    test_mult
    test_nested_if
    test_sin
    test_sin_unused_result_loc
    test_sr
    test_sub


### Native compile

Run *make*.


### Cross compile for PPC IFC1210

Make sure PPC tools are available for IFC1210 (EEE mounted on /opt/eldk-5.6),
then source PPC toolchain settings:

    source /opt/eldk-5.6/ifc1210/environment-setup-ppce500v2-linux-gnuspe

Run *make*.

### Executing test(s)

Command line options:

    Usage: test [-h] [-L loops] [-S size]
   
        -S unsigned int      Sample count (default: 300000) 
        -L unsigned int      Number of loops (default: 30) 
   
        -h                   Print this message 


Tests can be executed in several ways.

a) Run the test manually from command line (default parameters):

    ./test

b) Run the test manually from command line (custom parameters):

    ./test -S 50000
    ./test -L 300
    ./test -S 90000 -L 100

c) Using script *run_test.sh* (single test with different number of loops and
   sample count):

    bash run_test.sh test_add
    bash run_test.sh test_mult

Test parameters:

    S="300000 600000 1200000"
    L="1 10 30 50 100"

d) Using script *run_tests.sh* (limited set of tests with different number
   of loops and sample count):

    bash run_tests.sh

Test parameters:

    S="300000 600000 1200000"
    L="1 10 30 50 100"

Following tests are executed:

    test_add
    test_add_mul
    test_div
    test_div_sub
    test_mult
    test_sin
    test_sub
    test_sr
    test_if
    test_nested_if
    test_copy

e)  Using script *run_all_tests.sh* (run all tests with different number
    of loops and sample count):

    bash run_all_tests.sh

Test parameters:

    S="300000 600000 1200000"
    L="1 10 30 50 100"

### Logging results

Each test logs the duration and other test information into a CSV file called *tests.csv*.
Scripts check if the *tests.csv* exists and allow user to save previous results.

Example logged data:

    ID|HOST|CASE|DTYPE1|DTYPE2|SAMPLES|LOOPS|AVG|MAX|TOTAL|CPULOAD|NRCPUS|OPTFLAGS|OP
    1|hinkocmbp|test_add.c|double|double|300000|30|0.582|0.843|17.475|133|8|-O3|{ x[i] += (double) (y[i]); }

