#!/bin/bash

# run all tests with different number of loops and sample count

S="300000 600000 1200000"

T=$(make list | awk '/^TEST/ { print $2; }')
L="1 10 30 50 100"

if [ -f tests.csv ]; then
	echo "Log tests.csv exists. Use empty one?"
	read ans
	case $ans in
		y|Y)
			mv tests.csv "tests saved on $(date).csv"
	esac
fi

for l in $L; do
	for t in $T; do
		for s in $S; do
#			bash ./run_test.sh $t -L $l -S $s
			FF=$(find . -name "$t")
			for f in $FF; do
				./$f -L $l -S $s
			done
		done
	done
done

